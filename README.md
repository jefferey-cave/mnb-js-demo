# Markov Network Brain JS

A browser based Markov Network Brain system (fork of 
[new_think](https://github.com/pnealgit/new_think) )

Critters search for food using a network of logic gates.

A pure Javascript implementation of an MNB, suitable for learning how 
they work. Just open in your browser, hit `F12` and you can step through 
the logic to see how it works.

Please fork or suggest refactorings or updates in the project issues (I'm 
not particularily proud of the code, so you won't hurt my feelings)

## Run

1. Open `index.html`

You can pause the animation by clicking on it. History is retained so 
you can restart your browser

### Contained Server

> There is no need to use a server. 

However a basic server has been included for the cases where that is a 
`false` statement (thank-you GitLab WebIde).

```
npm start
```

## References

* [Chris Adami's ideas](http://adamilab.msu.edu/markov-network-brains/)
* DevoSoft has a [gentle introduction](http://devosoft.org/a-quick-introduction-to-markov-network-brains/)
* This is a fork of [new_think](https://github.com/pnealgit/new_think)

(please suggest others)

## Author's Note

Since debbugging and playing with this little application, I have 
spent more time philosophizing about the evolutionary nature of complex 
systems than ever before. I am not referring to biology but

* the way education and learning is acheived
* how teams in my workplace form and interact
* mechanisms of distribution of technologic insights
* geo-political problems 

While most MNB implementations go for bigger and more eye-catching (important 
for catching people's attention), this project goes small. This leaves the 
reader with an opportunity to hold the whole problem in their head and 
comprehend what they are observing on a very deep level.

It is the act of debugging a small app that offers the insight 
(and therefore the fun).
