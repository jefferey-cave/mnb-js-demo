import * as utils from './utils.js';

export{
	readState,
	saveState,
	destroy
};

const dataname='mnbHistory';
const compactionFreq = 100;
const digitSignificant = 3;
const protectHist = 135;
const cutoff = 2000;

function destroy(){
	localStorage.setItem(dataname,'[]');
}


function readState(){
	let data = localStorage.getItem(dataname);
	data = JSON.parse(data);
	if(!Array.isArray(data)){
		data = [];
	}
	return data;
}

function saveState(generation, genomes){
	if(generation < 1) return;
	let stats = genomes.map(vals=>{ return vals.pos - vals.neg; });
	stats = utils.calcStats(stats);

	stats.generation = generation;

	let row = {
		stats: stats,
		genomes: genomes,
	};
	let data = readState();
	if(data.length > 0 && data[0].stats.generation === row.stats.generation){
		return;
	}

	data.unshift(row);
	if(generation % compactionFreq === 0){
		data = compressData(data);
	}
	data = data
		.sort((a,b)=>{
			let compare = b.stats.generation - a.stats.generation;
			return compare;
		})
		.map((d,i)=>{
			if (i>10) delete d.genomes;
			return d;
		})
		.slice(0,cutoff)
		;

	data = JSON.stringify(data);
	localStorage.setItem(dataname,data);
	state.history.add([stats]);

}


function compressData(data){
	// Preserve the first few
	let rtn = data.slice(0,protectHist);

	// perform a reduce on the rest of them
	let scale = Math.max(state.generations.toString().length - digitSignificant,0);
	let divisor = Math.pow(10,scale);
	let results = data.slice(rtn.length);
	results = results.reduce((a,d)=>{
		d = d.stats;
		let key = Math.floor(d.generation / divisor) * divisor;
		let value = a[key] || {
			generation: key,
			max: Number.MIN_SAFE_INTEGER,
			min: Number.MAX_SAFE_INTEGER,
			avg: 0,
			count: 0,
			mv: 0,
			sd: 0
		};
		a[key] = value;
		
		// Aggregate stats
		// Including Standard Deviation: https://math.stackexchange.com/a/1853685
		let max = Math.max(value.max,d.max);
		let min = Math.min(value.min,d.min);
		let sumSq0 = value.count * (Math.pow(value.avg,2) + Math.pow(value.sd,2));
		let sumSq1 = d.count * (Math.pow(d.avg,2) + Math.pow(d.sd,2));
		let sumSq = sumSq0 + sumSq1;
		let count = value.count + d.count;
		let avg = value.avg * (value.count/count) + d.avg * (d.count/count);
		let mv = value.mv * (value.count/count) + d.mv * (d.count/count);
		let sd = sumSq/count - Math.pow(avg,2);
		sd = Math.pow(sd,0.5);
		
		// put the values in the object
		value.min = min;
		value.max = max;
		value.avg = avg;
		value.mv = mv;
		value.count = count;
		value.sd = sd;
		
		return a;
	},{});
	results = Object.values(results)
		.map((d)=>{
			return{
				stats: d
			};
		})
		.forEach(d=>{
			rtn.push(d);
		})
		;

	return rtn;
}
