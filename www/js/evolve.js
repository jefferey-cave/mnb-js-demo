import Bug from './things/bugs.js';
import Render from './render.js';

import * as utils from './utils.js';
import * as db from './database.js';

export{
	evolve
};

const _pickRandomGenome = new Array(10).fill(0);
function pickRandomGenome(range=state.bugs.length){
	let r = Math.random();
	r *= 2.5;
	let h = Math.tanh(r);
	h = 1-h;
	//_pickRandomGenome[Math.floor(h*_pickRandomGenome.length)]++;

	let i = Math.floor(h * range);
	return i;
}

function evolve(bugs) {
	const cull_rate = 0.1;
	// Keep approximately everything over 2 SD
	const keep_rate = 0.03;
	const clone_rate = 0.5;

	let runTimer = Date.now();

	bugs.forEach(bug=>{
		let reward = bug.reward - bug.MIN_REWARD;
		if(state.select!=='soft'){
			reward *= Math.min(Math.round(bug.experience.hasActed || 0),1);
		}

		bug.experience.karma =  (bug.experience.karma || reward);
		bug.experience.karma += reward;
		bug.experience.karma /= 2;

		bug.experience.karma =  Math.floor(bug.experience.karma);
	});

	db.saveState(state.generations++, bugs.map(bug=>{
		let val = {
			gen: bug.genome.toString(),
			pos: bug.experience.positive / bug.MAX_REWARD,
			neg: bug.experience.negative / bug.MAX_REWARD,
			kar: bug.experience.karma / bug.MAX_REWARD,
		};
		return val;
	}));


	// cull the hurd
	let cull = state.bugQty - Math.floor(cull_rate * state.bugQty) - 1;
	cull = Math.min(cull,bugs.length-1);
	let keep = Math.floor(keep_rate * state.bugQty);
	let stats = utils.calcStats(bugs.map(bug=>{ return bug.reward; }));
	//let killVal = (Math.max(stats.avg-stats.sd,stats.min) || Number.MIN_SAFE_INTEGER)+1;
	let killVal = stats.min;
	let keepVal =  (Math.min(stats.avg+stats.sd,stats.max) || Number.MIN_SAFE_INTEGER)+1;

	bugs.sort((a, b)=>{return b.experience.karma - a.experience.karma;});

	if(bugs.length){
		while(keep < cull && bugs[keep].experience.karma >= keepVal) keep++;
		while(cull > keep && bugs[cull].experience.karma <= killVal) cull--;
		if(cull <= keep){
			cull = keep+1;
		}
		bugs = bugs.slice(0,cull);
	}

	let reject = 0;
	let origgen = bugs.map(b=>{return b.genome;});
	while(bugs.length < state.bugQty){
		let gen = pickRandomGenome(origgen.length);
		let kar = 0;
		if(bugs[gen]){
			kar = bugs[gen].experience.karma;
		}
		gen = origgen[gen];
		let bug = new Bug(gen);
		bug.experience.karma = kar;
		reject++;
		if(0.8 < Math.random() || WillAct(bug)){
			bugs.push(bug);
			reject--;
		}
	}

	for(let i = keep; i < bugs.length; i++){
		let bug = bugs[i];
		// Sexually reproduce
		if (Math.random() > clone_rate) {
			let s = pickRandomGenome(origgen.length);
			let kar = bugs[s].experience.karma;
			let sperm = origgen[s];
			bug.genome = bug.genome.mate(sperm);
			bug.experience.karma = (bug.experience.karma+kar) / 2;
		}
		bug.genome = bug.genome.mutate();
	}

	//clear for next round
	//state.bugs.sort((a, b)=>{return b.experience.karma - a.experience.karma;});
	for (let bug of bugs) {
		bug.init();
	}

	runTimer = Math.floor(Date.now() - runTimer);
	stats = {
		'score': stats,
		'karma': utils.calcStats(bugs.map(bug=>{ return bug.experience.karma; }))
	}
	LogAnalytics(state.generations, {
		//'generation': state.generation,
		'cull': cull,
		'keep': keep,
		'reject': reject,
		'scoreμ': stats.score.avg,
		'scoreσ': stats.score.sd,
		'karmaμ': stats.karma.avg,
		'karmaσ': stats.karma.sd,
	});

	Render.charts();

	//console.debug("random");
	//new Array(1000).fill(100).forEach(()=>{pickRandomGenome(100);});
	//console.debug(_pickRandomGenome);

	return bugs;
}


async function LogAnalytics(generation, events){
	const label = `gen[${generation}]`;
	events = Object.entries(events)
	for(let e of events){
		let key = e[0];
		let val = e[1];
		val = {
			event_category: 'evolve',
			event_label: label,
			value: Math.floor(Math.max(0,val)),
			non_interaction: true
		};
	}
}


function WillAct(bug){
	if(state.select === 'soft') return true;

	let min = bug.actuators.byteOffset;
	let max = min + bug.actuators.length-1;
	for(let gate of bug.brain.gates){
		for (let output of gate.outputs) {
			if(output >= min && output <= max){
				return true;
			}
		}
	}
	return false;
}
