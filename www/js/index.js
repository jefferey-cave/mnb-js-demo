import './utils.js';
import './views/ui-elements.js';

import Food from './things/food.js';
import Bug from './things/bugs.js';
import Universe from './things/universe.js';

import Render from './render.js';

import * as evolve from './evolve.js';
import * as gates from './mnb/gates.js';
import * as db from './database.js';

function loadParams(params = {}){
	let rtn = {
		animate: true,
		animation_count_limit: 2000,
		bugQty: 50,
	};
	rtn = Object.assign(rtn,params);
	params = new URLSearchParams(window.location.search);
	for(let p of params.entries()){
		rtn[p[0]] = p[1];
	};
	return rtn;
}

function restore(){
	let history = db.readState();
	if(!history.length) return;

	state.generations = history[0].stats.generation;
	state.bugs = history[0].genomes.map(d=>{
		let bug = new Bug(d.gen);
		//let verify = bug.genome.toString();
		//console.assert(d.gen === verify,'Error restoring genome from string.');
		bug.experience.positive = d.pos * bug.MAX_REWARD;
		bug.experience.negative = d.neg * bug.MAX_REWARD;
		bug.experience.karma = (d.kar || 0) * bug.MAX_REWARD;
		return bug;
	});

	history = history.map(d=>{
		let stats = d.stats;
		return stats;
	});
	state.history.add(history);
}

function initFood(qty) {
	state.food = new Array(qty).fill(null).map((d,i)=>{
		let food = new Food();
		food.x = (i+1) / (qty+1);
		return food;
	});
}

function init(){

	state = loadParams(state);
	state.animation_count = 0;
	state.gatetypes = gates.GateTypes.bitGateTypes;
	state.generations = 0;
	state.history = crossfilter([]);

	//create the array of circles that will be animated
	restore();
	initFood(5);
	state.terranium = new Universe(state);

	let scan = document.querySelector('life-brainscan');
	scan.terranium = state.terranium;
	let terr = document.querySelector('life-terranium');
	terr.terranium = state.terranium;

	document.querySelector('button[name="reset"]').addEventListener('click', ()=>{
		db.destroy();
		window.location.reload();
	});

	state.terranium.addEventListener('complete',()=>{
		let food = state.terranium.lifeForms.filter(l=>{ return l.Type === 'Food'; });
		let bugs = state.terranium.lifeForms.filter(l=>{ return l.Type === 'Bug';  });
		bugs = evolve.evolve(bugs);
		state.terranium.clear();
		state.terranium.add(food);
		state.terranium.add(bugs);
		setTimeout(()=>{
			state.terranium.run();
		},500);
	});
	state.terranium.run(1);
	scan.Render();
	terr.Render();
	Render.charts();
}

//this gets it all started
window.addEventListener('load', () => {
	setTimeout(init, 1);
});

