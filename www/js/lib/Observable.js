export {
	Watchable as default
};

class Watchable extends EventTarget {
	constructor(object = null){
		super();
		if(!object){
			object = {};
		}
		
		this._ = {
			proxy: new Proxy(object,handler)
		};
	}
	
	_handler(){
	}
}

const handler = {
	defineProperty: function(obj,prop,value){
		
	},
	deleteProperty: function(obj,prop){
		
	},
	set: function(obj,prop,value){
		if(prop.startsWith('_')){
			return;
		}
		if(obj._.proxy[prop] === value){
			return;
		}
		let proceed = this.dispatchEvent(new CustomEvent('change',{detail:{oldVal:obj._.proxy[prop],newVal:value}}));
		if(proceed){
			obj._.proxy[prop] = value;
		}
	},
	get: function(obj,prop){
		if(prop.startsWith('_')){
			return undefined;
		}
		return obj._.proxy[prop];
	}
}