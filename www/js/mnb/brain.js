import {rangeWrap} from '../utils.js';

export{
	Brain as default
};

const max_number_gates = 80;
const min_number_gates = 10;
const min_number_inputs = 2;
const max_number_inputs = 4;
const min_number_outputs = 1;
const max_number_outputs = 4;
const min_brainSize = 50;
const max_brainSize = 100;

class Brain{
	constructor(genome){
		this.state = CreateMemory(genome);
		this.gates = CreateGates(genome, this.state);
	}

	think(sensorData=[]) {
		let mind = this.state;
		for (let k = 0; k < sensorData.length; k++) {
			mind[k] = sensorData[k];
		}

		for (let gate of this.gates) {
			let result = gate.handler(gate, mind);
			for (let o of gate.outputs) {
				mind[o] = result;
			}
		}

		return mind;
	}

	get hash(){
		let rgbSize = Math.pow(2,3*8);
		let rgb = this.state.reduce((a,d)=>{
				a *= 2;
				a += Math.floor(a/rgbSize);
				a %= rgbSize;
				a ^= d;
				return a;
			},0);
		return rgb;
	}
}

class Gate {
	constructor(genome,memory){
		this.type = genome.nextRatio;
		this.type *= state.gatetypes.length;
		this.type = Math.floor(this.type);

		this.inputs = wireNeurons(genome, memory.length, min_number_inputs, max_number_outputs);
		this.outputs = wireNeurons(genome, memory.length, min_number_outputs, max_number_outputs);

		this.handler = state.gatetypes[this.type];
	}
}


function CreateGates(genome,memory) {
	let qty = genome.nextRatio;
	qty *= max_number_gates - min_number_gates;
	qty += min_number_gates;
	qty = Math.floor(qty);

	let gates = [];
	for(let i = 0; i<qty; i++){
		let gate = new Gate(genome,memory);
		gates.push(gate);
	}
	return gates;
}


function CreateMemory(genome) {
	let qty = genome.nextRatio;
	qty *= max_brainSize - min_brainSize;
	qty += min_brainSize;
	qty = Math.floor(qty);

	let buf = 0;
	let bufSize = 0;
	let states = [];
	for (let k = 0; k < qty; k++) {
		if(!bufSize){
			buf = genome.next;
			bufSize = genome.GeneScaleBits;
		}
		let state = buf % 2;
		states.push(state);
		bufSize--;
	}

	states = new Uint8ClampedArray(states);
	return states;
}


function wireNeurons(genome, mindSize, minWirings, maxWirings) {
	let qty = genome.nextRatio;
	qty *= maxWirings - minWirings;
	qty += minWirings;
	qty = Math.floor(qty);

	let wirings = [];
	for(let i = 0; i<qty; i++){
		let w = genome.nextRatio;
		w *= mindSize;
		w = Math.floor(w);
		wirings.push(w);
	}
	return wirings;
}
