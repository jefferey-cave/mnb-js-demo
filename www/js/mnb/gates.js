export {
	GateTypes
};

const GateTypes = {
	bitGateTypes: [
		xor_gate,
		or_gate,
		and_gate,
		nor_gate,
		nand_gate
	]
};


function xor_gate(g1, state) {
	var sum = 0;
	var val = 0;
	for (var k = 0; k < g1.inputs.length; k++) {
		sum += state[g1.inputs[k]];
	}
	if (sum == 1) {
		val = 1;
	}
	return val;
}


function or_gate(g1, state) {
	var sum = 0;
	var val = 0;
	for (var k = 0; k < g1.inputs.length; k++) {
		sum += state[g1.inputs[k]];
	}
	if (sum > 0) {
		val = 1;
	}
	return val;
}


function and_gate(g1, state) {
	var sum = 0;
	var val = 0;
	for (var k = 0; k < g1.inputs.length; k++) {
		sum += state[g1.inputs[k]];
	}
	if (sum == g1.inputs.length) {
		val = 1;
	}
	return val;
}


function nor_gate(g1, state) {
	var sum = 0;
	var val = 1; //note inversion here
	for (var k = 0; k < g1.inputs.length; k++) {
		sum += state[g1.inputs[k]];
	}
	if (sum > 0) {
		val = 0;
	}
	return val;
}


function nand_gate(g1, state) {
	var sum = 0;
	var val = 0;
	for (var k = 0; k < g1.inputs.length; k++) {
		sum += state[g1.inputs[k]];
	}
	if (sum > 0) {
		val = 1;
	}
	return val;
}

