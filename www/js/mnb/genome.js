import * as utils from '../utils.js';

export{
	Genome as default
};


const mutation_rate = 0.2;
const mutation_size = 0.01;
const lenGenome = 3000;


class Genome extends Uint16Array {
	constructor(genes){
		genes = genes || lenGenome;
		if(typeof genes === 'string'){
			let bytes = utils.B64ToUInt8(genes);
			genes = bytes.buffer;
		}
		super(genes);

		if(typeof genes === 'number'){
			for (let i=this.length-1; i >= 0; i--) {
				let r = Math.floor(Math.random() * Genome.MAX_GENE);
				this[i] = r;
			}	
		}
		this.pos = 0;
	}

	get GeneScale(){
		return Genome.MAX_GENE;
	}
	get GeneScaleBits(){
		return 8*Genome.BYTES_PER_ELEMENT;
	}
	
	get peek(){
		let val = this.pos;
		val = this[val];
		val = Math.floor(val);
		return val;
	}
	get peekRatio(){
		let val = this.peek;
		val = val / this.GeneScale;
		return val;
	}
	
	get next(){
		let val = this.peek;
		this.pos++;
		return val;
	}
	get nextRatio(){
		let val = this.next;
		val /= this.GeneScale;
		return val;
	}
	
	get pos(){
		return this._pos;
	}
	set pos(val){
		this._pos = (Math.floor(val) + this.length) % this.length;
	}
	
	shift(){
		return this.next;
	}
	
	pop(){
		return this.next;
	}
	
	push(){
		console.warn("Illegal Operation: PUSH to GENOME");
	}
	
	unshift(){
		console.warn("Illegal Operation: UNSHIFT to GENOME");
	}
	
	slice(){
		return new Genome(super.slice());
	}
	
	clone(){
		return this.slice();
	}
	
	mutate() {
		let genome = this.clone();
		for (let m = Math.floor(Math.random()*this.length); m>=0; m--) {
			if(Math.random() > mutation_rate) continue;
			
			let loc = Math.floor(Math.random() * this.length);
			let val = (Math.random()-0.5);
			val	*= maxGene;
			val *= mutation_size;
			val  = Math.floor(val);
			val += genome[loc];
			val += maxGene;
			val %= maxGene;
			genome[loc] = val % maxGene;
		}
		return genome;
	}
	
	mate(that) {
		let rtn = this.clone();
		that = that || rtn;
		let section_length = Math.randomIntFromInterval(100, 1000);
		let pos = Math.floor(Math.random() * (this.length - (section_length + 1)));
		let stop = pos + section_length;
		if (stop > this.length) {
			stop = this.length - 5;
		}
		for (; pos < stop; pos++) {
			rtn[pos] = that[pos];
		}
		return rtn;
	}

	toString(){
		/*
		let a = JSON.stringify(new Array(20).fill(0).map(d=>{return Math.floor(Math.random()*256);}));
		let b = JSON.parse(a);
		b = new Uint8Array(b);
		b = utils.UInt8ToB64(b);
		b = utils.B64ToUInt8(b);
		b = Array.from(b);
		b = JSON.stringify(b);
		console.log('Valid : ' + (a === b));
		*/
		
		let bytes = new Uint8Array( this.buffer );
		bytes = utils.UInt8ToB64(bytes);
		return bytes;
	}
}


Genome.MAX_GENE = Math.pow(2,8*Genome.BYTES_PER_ELEMENT);
const maxGene = Genome.MAX_GENE;
