export {
	Render as default
};

const cAquarium = 'WhiteSmoke';
const cLifeBorder = 'SlateGray';
const cFoodColor = 'forestgreen';
const cAntena = 'DarkSlateGray';

class Render{
	
	static charts(){
		if(!Render.timeseries){
			Render.timeseries = dc.lineChart('#chart');
			Render.datatable = dc.dataTable('#datatable');


			let dim = state.history.dimension((d)=>{
				return d.generation;
			});

			let min = dim.group().reduceSum((d)=>{
				return d.min;
			});
			let sdSub = dim.group().reduceSum((d)=>{
				return d.avg-d.sd;
			});
			let avg = dim.group().reduceSum((d)=>{
				return d.sd;
			});
			let sdAdd = dim.group().reduceSum((d)=>{
				return +d.sd;
			});
			let max = dim.group().reduceSum((d)=>{
				return d.max-(d.sd*2);
			});
			const RewardFormatter = d3.format('.2s');
			Render.timeseries
				.width(600)
				.height(600)
				.margins({top: 30, right: 50, bottom: 25, left: 40})
				.x(d3.scaleLinear())
//				.y(d3.scaleLog())
				.elasticY(true)
				.elasticX(true)
				.curve(d3.curveBasis)
//				.curve(d3.curveLinear)
				.renderArea(true)
				.brushOn(false)
				.renderDataPoints(true)
				.clipPadding(10)
				.dimension(dim)
//				.group(min)
				.group(sdSub)
				.stack(avg)
				.stack(sdAdd)
//				.stack(max)
				.ordinalColors(['steelblue','steelblue','steelblue','#984ea3','#ff7f00','#ffff33','#a65628'])
				;
			Render.timeseries.yAxis().tickFormat(RewardFormatter);

			Render.datatable
				.width(600)
				.height(600)
				.dimension(dim)
				.size(15)
				.showSections(false)
				.columns([
					'generation',
					'count',
					{label:'Min',format:(d)=>{return RewardFormatter(d.min);}},
					{label:'Avg',format:(d)=>{return RewardFormatter(d.avg);}},
					{label:'Sd',format:(d)=>{return RewardFormatter(d.sd);}},
					{label:'Max',format:(d)=>{return RewardFormatter(d.max);}},
					{
						label:'Whisker',
						format:(d)=>{
							let range = d.max - d.min;
							let sdLow = 100*(d.avg-d.sd-d.min)/range;
							let avg = 100*(d.avg-d.min)/range - sdLow;
							let sdHigh = 100*(d.avg+d.sd-d.min)/range - avg;
							let max = 100 - sdHigh;
							let html = `<table width='100em' style='border:0px'><tr><td width='${sdLow}%' style='border-width:0px;border-left:1px solid black;'><hr /></td><td width='${avg}%'></td><td width='${sdHigh}%'></td><td width='${max}%' style='border-width:0px;border-right:1px solid black;'><hr /></td></tr></table>`;
							return html;
						}
					},
				])
				.sortBy(function (d) { 
					return [(+d.generation).toString().padStart(10,'0')]; 
				})
				.order(d3.descending)
				//.on('preRender', update_offset)
				//.on('preRedraw', update_offset)
				//.on('pretransition', display)
				;

			dc.renderAll();
		}
		dc.redrawAll();
	}
	
}

