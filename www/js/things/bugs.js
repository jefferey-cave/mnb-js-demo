import LifeForm from './lifeform.js';

import * as utils from '../utils.js';

export{
	Bug as default
};

class BrainIO extends Uint8ClampedArray{
	constructor(brain,start,end){
		super(brain.state.buffer,start,end-start+1);
	}
	reset(value){
		this.fill(value);
	}
}

class Bug extends LifeForm{

	constructor(genome=null) {
		super(genome);

		this.r *= 10;
		this._antennaLen = this.r*2;
		if(!this._antennaLen){
			debugger;
		}

		this.speed = 0;
		this.pos = new utils.Cartesian([0,0,0]);

		this.init();
	}

	get MAX_SPEED(){
		return 3*this.PIXEL;
	}
	get MIN_SPEED(){
		return 0*this.PIXEL;
	}

	init() {
		super.init();
		
		// place it randomly on the board
		let r = Math.random()*0.2+0.2;
		let coords = utils.Conversions.polarToCartessian(Math.random(),r);
		this.x = coords.x + 0.5;
		this.y = coords.y + 0.5;
		this.speed = (this.MAX_SPEED + this.MIN_SPEED) / 2;
		this.dir = Math.random();
		
		// initialize it's sensors
		let brain = this.brain;
		this.sensors = new class extends BrainIO{
			constructor(){ super(brain,0,10); }
			set posX(v)  { this[ 0] = v; }
			set posY(v)  { this[ 1] = v; }
			set speed(v) { this[ 2] = v; }
			set dir(v)   { this[ 3] = v; }
			set food(v)  { this[ 4] = v; }
			set antL(v)  { this[ 5] = v; }
			set antF(v)  { this[ 6] = v; }
			set antR(v)  { this[ 7] = v; }
			set painL(v) { this[ 8] = v; }
			set painF(v) { this[ 9] = v; }
			set painR(v) { this[10] = v; }
			get speed() { return this[ 2]; }
			get dir()   { return this[ 3]; }
			get food()  { return this[ 4]; }
			get antL()  { return this[ 5]; }
			get antF()  { return this[ 6]; }
			get antR()  { return this[ 7]; }
			get painL() { return this[ 8]; }
			get painF() { return this[ 9]; }
			get painR() { return this[10]; }
		};
		this.actuators = new class extends BrainIO{
			constructor() { super(brain,11,13); }
			get speed() { return this[0]; }
			get left()  { return this[1]; }
			get right() { return this[2]; }
		};

		this.actuators.reset();
		this.check_for_wall();
		this.experience.negative = 0;
		this.experience.positive = 0;
	}
	
	clone() {
		let genome = this.genome.clone();
		let bug = new Bug(genome);
		return bug;
	}

	toString() {
		return JSON.stringify(this);
	}

	static fromString(json) {
		json = JSON.parse(json);
		let bug = new Bug();
		for (let name in json) {
			if (name in bug) {
				bug[name] = json[name];
			}
		}
	}

	get x(){
		this.pos = this.pos || new utils.Cartesian([0,0,0]);
		return this.pos.x;
	}
	get y(){
		this.pos = this.pos || new utils.Cartesian([0,0,0]);
		return this.pos.y;
	}

	set x(val){
		if(val === this.x) return;
		this.pos[0] = val;
		this._.antennae = null;
	}
	set y(val){
		if(val === this.y) return;
		this.pos[1] = val;
		this._.antennae = null;
	}
	
	get antennae() {
		if(this._.antennae) return this._.antennae;

		this._.antennae = [-0.25 , 0 , 0.25].map(a=>{
			let offset = utils.Conversions.polarToCartessian(this.dir+a,this.antennaLen);
			let val = {
				x: this.x + offset.x,
				y: this.y + offset.y,
			};
			return val;
		});

		return this._.antennae;
	}

	think(){
		//this.actuators.reset();
		this.brain.think();
	}

	act(){
		// change speed by -1, or 1 units
		let speed = this.actuators.speed;
		speed *= 2;
		speed -= 1;
		speed *= this.PIXEL;
		// potentially no change to speed
		//speed *= brainstate[SPEED+1];
	
		// change the direction by +/- 45 degrees
		let dir = (this.actuators.right-this.actuators.left) ;
		dir /= 2;
		dir *= 0.0078125;

		// calculate the direction
		this.speed += speed;
		this.dir += dir;
	}


	measure(){
		this.sensors.reset();
		this.senseLocation();
		this.check_for_wall();
		this.senseFood();

		this.applyReward();
	}


	applyReward(){
		this.reward += 50 * this.sensors.food;
		this.reward += 10 * 
			[this.sensors.antL,this.sensors.antF,this.sensors.antR]
			.reduce((a,d)=>{return Math.min(a+d,1);},0)
			;
		this.reward -= 1 * 
			[this.sensors.painL,this.sensors.painF,this.sensors.painR]
			.reduce((a,d)=>{return Math.min(a+d,1);},0)
			;
		
		if(this.old_actuators){
			if(!this.experience.hasActed){
				let acted = this.actuators.reduce((a,d,i)=>{
					return a + d * Math.pow(2,i);
				},0);
				this.experience.hasActed = 0;
				if(this.old_actuators !== acted){
					this.experience.hasActed = 1;
				}
			}
		}
		else{
			this.old_actuators = this.actuators.reduce((a,d,i)=>{
				return a + d * Math.pow(2,i);
			},0);
		}
	}


	senseFood() {
		for (let food of state.food) {
			let collide = food.isCollision(this.x,this.y,this.r);
			if (collide) {
				this.sensors.food = 1;
			}
			
			//check for antenna collision with food
			let ant = this.antennae.map((antenna)=>{
				let collide = food.isCollision(antenna.x,antenna.y,0);
				let hit = (collide) ? 1 : 0 ;
				return hit;
			});
			this.sensors.antL = ant[0];
			this.sensors.antF = ant[1];
			this.sensors.antR = ant[2];
		}
	}

	senseLocation() {
		this.sensors.posX = this.x;
		this.sensors.posY = this.y;
		this.sensors.speed = Math.ceil(this.speed) % 1;
		this.sensors.dir = this.dir;

		this.experience.positive += this.speed / this.PIXEL;
	}

	check_for_wall() {
		let universe = state.terranium;
		if(!universe) return;
		// if the bug hit the wall ...
		let polar = utils.Conversions.cartessianToPolar(this.x-0.5,this.y-0.5);
		if(polar.distance+this.r >= 0.5){
			// hard stop
			let cart = utils.Conversions.polarToCartessian(polar.angle,0.5-this.r);
			this.x = cart.x+0.5;
			this.y = cart.y+0.5;
			this.speed = 0;
			// electric fence
			this.reward -= 5;
		}
		
		//check for antenna collision
		let ant = this.antennae.map((a)=>{
			let felloff = !universe.isCollision(a.x,a.y,0);
			let hit = (felloff) ? 1 : 0;
			return hit;
		});
		this.sensors.painL = ant[0];
		this.sensors.painF = ant[1];
		this.sensors.painR = ant[2];
	}

}
