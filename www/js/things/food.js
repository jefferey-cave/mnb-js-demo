import LifeForm from './lifeform.js';

export{
	Food as default
};

class Food extends LifeForm{
	constructor() {
		super();
		
		this.r *= 20;
		this.x = (this.id%4) * 0.25 * this.r;
		this.y = 0.5;
	}

}

