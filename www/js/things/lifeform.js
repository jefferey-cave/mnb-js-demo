import Thing from './thing.js';
import Genome from '../mnb/genome.js';
import Brain from '../mnb/brain.js';

export {
	LifeForm as default
};

class LifeForm extends Thing{
	constructor(genome=null) {
		super();
		if (!LifeForm.births) LifeForm.births = 0;

		this.id = LifeForm.births++;
		this.genome = new Genome(genome);
		this._antennaLen = 0;
		this.experience = {};

		this.init();
	}

	get Type(){
		return this.constructor.name;
	}


	init(){
		// Rebuild the animal's brain
		this.genome.pos = 0;
		this.brain = new Brain(this.genome);
		this.experience.positive = 0;
		this.experience.negative = 0;
	}

	get antennaLen(){
		return this._antennaLen;
	}
	get antenna_length(){
		console.warn('Deprecated function');
		return this.antennaLen;
	}

	get MAX_SPEED(){
		return 0*this.PIXEL;
	}
	get MIN_SPEED(){
		return 0*this.PIXEL;
	}

	get MIN_REWARD(){
		return -LifeForm.MAX_REWARD;
	}
	get MAX_REWARD(){
		return LifeForm.MAX_REWARD;
	}

	get reward(){
		let value = this.experience.positive - this.experience.negative;
		return value;
	}
	set reward(amt){
		amt = Math.floor(amt);
		if(amt === this.reward) return;
		this.emitChange('reward',this.reward,amt);
		amt -= this.reward;
		if(amt < 0){
			this.experience.negative -= amt;
			this.experience.negative = Math.min(this.MAX_REWARD, this.experience.negative);
			this.experience.negative = Math.floor(this.experience.negative);
		}
		else{
			this.experience.positive += amt;
			this.experience.positive = Math.min(this.MAX_REWARD, this.experience.positive);
			this.experience.positive = Math.floor(this.experience.positive);
		}

	}

	get speed(){
		return this._speed || 0;
	}
	set speed(val){
		val = Math.min(val,this.MAX_SPEED);
		val = Math.max(val,this.MIN_SPEED);
		if(this._speed === val) return;
		this.emitChange('speed',this._speed,val);
		this._speed = val;
	}
	get dir(){
		return this._dir || 0;
	}
	set dir(val){
		val -= Math.floor(val);
		if(this._dir=== val) return;
		this.emitChange('dir',this._dir,val);
		this._dir = val;
	}

	measure(){}
	think(){}
	act(){}

}

LifeForm.MAX_REWARD = Math.pow(2,15);
