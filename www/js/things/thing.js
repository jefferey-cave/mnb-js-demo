export {
	Thing as default
};

class Thing extends EventTarget{
	constructor(opts = {}) {
		super();
		this._ = {};
		this._.changes = new Set();
		this.r = Thing.PIXEL;
		this.x = Math.random();
		this.y = Math.random();

		this._.gtag = opts.gtag || (()=>{});
	}

	get PIXEL(){
		return Thing.PIXEL;
	}

	isCollision(x, y, r=0) {
		let dist = Math.hypot(x - this.x, y - this.y);
		dist -= this.r + r;
		let collide = (dist <= 0);
		return collide;
	}
	isContained(x,y,r=0){
		return this.isCollision(x,y,-r);
	}

	emitChange(prop){
		this._.changes.add(prop);
		if(!this._.emitting){
			this._.emitting = setTimeout(()=>{
				this._.emitting = false;
				var event = new CustomEvent("change", {
					detail: Array.from(this._.changes),
				});
				this.dispatchEvent(event);
				this._.changes.clear();
			},16);
		}
	}

}

Thing.PIXEL = (1/1024);
//Thing.PIXEL = (1/600);
