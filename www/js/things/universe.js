import Thing from './thing.js';
import LifeForm from './lifeform.js';

import * as utils from '../utils.js';

export {
	Universe as default
};

class Universe extends Thing {
	constructor(settings) {
		super(settings);
		this.x = 0.5;
		this.y = 0.5;
		this.r = 0.5;

		this.settings = settings;
		this.defaultTicks = settings.animation_count_limit;
		this.totalTicks = this.defaultTicks;
		this.ticks = 0;
		this._lifeForms = new Set();
		this._running = null;
		this._.best = null;
		this._.worst = null;

		this.addEventListener('complete',()=>{
			this.ticks = 0;
		});

		this._.lifeChange = (event)=>{
			if(event.detail.includes('reward')){
				if(this.best.experience.positive < event.target.experience.positive){
					this.best = event.target;
				}
				if(this.worst.experience.negative < event.target.experience.negative){
					this.worst = event.target;
				}
			}
		};

		this.add(settings.food);
		this.add(settings.bugs);

	}
	
	get best(){
		return this._.best;
	}
	set best(life){
		if(this._.best === life) return;
		this._.best = life;
		this.emitChange('best');
	}
	get worst(){
		return this._.worst;
	}
	set worst(life){
		if(this._.worst === life) return;
		this._.worst = life;
		this.emitChange('worst');
	}

	get lifeForms(){
		return Array.from(this._lifeForms.values());
	}

	add(life){
		if(!Array.isArray(life)){
			return this.add([life]);
		}
		else{
			for(let l of life){
				if(l instanceof LifeForm){
					if(!this._lifeForms.size){
						this.best = l;
						this.worst = l;
					}
					this._lifeForms.add(l);
					l.addEventListener('change',this._.lifeChange);
					l.emitChange('reward');
					this.dispatchEvent(new CustomEvent("add", {detail: l}));
				}
			}
		}
		return this;
	}

	remove(life){
		if(Array.isArray(life)){
			life.forEach(l=>{this.remove(l);});
			return;
		}
		this._lifeForms.delete(life);
		life.removeEventListener('change',this._.lifeChange);
		if(this.best === life){
			this.best = [...this._lifeForms].reduce((a,d)=>{
				a = (a===null || a.experience.positive < d.experience.positive) ? d : a;
				return a;
			},null);
		}
		if(this.worst === life){
			this.worst = [...this._lifeForms].reduce((a,d)=>{
				a = (a===null || a.experience.negative < d.experience.negative) ? d : a;
				return a;
			},null);
		}
		this.dispatchEvent(new CustomEvent("remove", {detail: life}));
	}

	clear(){
		this.remove(this.lifeForms);
	}

	run(ticks = this.defaultTicks){
		if(this._running) return this._running;

		this.totalTicks = ticks || this.defaultTicks;
		this._.gtag('event', 'start', {
			event_category: 'generation', 
			event_label: 'moments',
			value: this.totalTicks
		});
		this._.gtag('event', 'start', {
			event_category: 'generation', 
			event_label: 'lifeforms',
			value: this.lifeForms.length
		});
		let runTimer = performance.now();

		let ticker = null;
		let self = this;
		function ticktime(){
			let didIncrement = self.incrementTime();
			if(didIncrement){
				ticker = setTimeout(ticktime,1);
			}
		};

		function finishedRun(resolve){
			runTimer = performance.now() - runTimer;
			self._.gtag('event', 'timing_complete', {
				'event_category': 'generation', 
				'name': 'runTime', 
				'value': runTimer
			});
			self.removeEventListener('complete',finishedRun);
			clearTimeout(ticker);
			resolve(this);
			self._running = null;
		}

		this._running = new Promise((resolve,reject)=>{
			this.addEventListener('complete',()=>{
				finishedRun(resolve);
			});
			ticktime();
		});

		return this._running;
	}

	incrementTime(){
		if(this.ticks>=this.totalTicks){
			this.dispatchEvent(new CustomEvent('complete'));
			return false;
		}
		this.ticks++;
		for (let bug of this.lifeForms) {
			// read sensor input in order
			bug.measure();
			// think about the results
			bug.think();
			// apply outputs
			bug.act();

			this.move(bug);
		}

		if(this.ticks % 10 === 0){
			let prog = new ProgressEvent('progress', {
				lengthComputable: true,
				loaded: this.ticks,
				total: this.totalTicks
			});
			this.dispatchEvent(prog);
		}

		return this.ticks;
	}

	move(life){
		let offset = utils.Conversions.polarToCartessian(life.dir,life.speed);
		life.x += offset.x;
		life.y += offset.y;
	}

}
