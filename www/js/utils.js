export{
	Conversions,
	calcStats,
	rangeWrap,
	UInt8ToB64,
	B64ToUInt8,
	Cartesian,
	Polar
};

if(!Math.randomIntFromInterval) Math.randomIntFromInterval = (min, max) => {
	return Math.floor(Math.random() * (max - min + 1) + min);
};

  

function rangeWrap(val, lower, upper){
	let range = Math.abs(upper-lower);
	
	//val -= lower;
	//val %= range;
	//val += lower;
	if(range === 0){
		val = lower;
	}
	else{
		val = (val-lower) % range + lower;
	}

	return val;
}

class Cartesian extends Array{
	constructor(a=[]){
		super();
		if(a instanceof Array){
			for(let c of a){
				this.push(c);
			}
		}
	}

	get x(){
		return this[0];
	}
	get y(){
		return this[1];
	}
	get z(){
		return this[2];
	}
}

class Polar extends Array{
	constructor(a=[]){
		super();
		if(a instanceof Array){
			for(let c of a){
				this.push(c);
			}
		}
	}
	get angle(){
		return this[1];
	}
	set angle(value){
		this[1] = value;
	}
	get distance(){
		return this[0];
	}
	set distance(value){
		this[0] = value;
	}
}

class Conversions{
	/**
	 * 
	 * @param {*} angle Proportion of a circle to rotate (1.0 = 360deg)
	 * @param {*} dist  
	 */
	static polarToCartessian(angle,dist){
		angle -= Math.floor(angle);
		angle *= Math.PI * 2;
		let x = dist * Math.cos(angle);
		let y = dist * Math.sin(angle);

		return new Cartesian([x,y]);
	}

	static cartessianToPolar(x,y){
		let dist = Math.hypot(x,y);
		let angle = Math.atan2(y, x);

		//convert from radians to proportion of circumference
		angle /= Math.PI * 2;
		angle = angle - Math.floor(angle);
		angle += 1;
		angle = angle - Math.floor(angle);

		let value = new Polar([dist,angle]);
		return value;
	}
}


function calcStats(values){
	let stats = {
		min: +Number.MAX_VALUE,
		max: -Number.MAX_VALUE,
		avg: 0,
		sd: 0,
		mv: 0,
		count: 0
	};
	stats.count = values.length;
	for (let val of values) {
		stats.avg += val;
		stats.max = Math.max(stats.max, val);
		stats.min = Math.min(stats.min, val);
	}
	stats.avg = stats.avg / stats.count;
	for (let val of values) {
		let variance = val - stats.avg;
		stats.sd += Math.pow(variance, 2);
		stats.mv += Math.abs(variance);
	}
	stats.sd = Math.pow(stats.sd/stats.count, 0.5);
	stats.mv = stats.sd/stats.count;
	
	return stats;
}




/*\
|*|
|*|  Base64 / binary data / UTF-8 strings utilities (#1)
|*|
|*|  https://developer.mozilla.org/en-US/docs/Web/API/WindowBase64/Base64_encoding_and_decoding
|*|
|*|  Author: madmurphy
|*|
\*/

/* Array of bytes to base64 string decoding */

function b64ToUint6 (nChr) {

	return nChr > 64 && nChr < 91 ?
		nChr - 65
	  : nChr > 96 && nChr < 123 ?
		nChr - 71
	  : nChr > 47 && nChr < 58 ?
		nChr + 4
	  : nChr === 43 ?
		62
	  : nChr === 47 ?
		63
	  :
		0;
  
  }
  
  function B64ToUInt8 (sBase64, nBlockSize) {
  
	var
	  sB64Enc = sBase64.replace(/[^A-Za-z0-9\+\/]/g, ""), nInLen = sB64Enc.length,
	  nOutLen = nBlockSize ? Math.ceil((nInLen * 3 + 1 >>> 2) / nBlockSize) * nBlockSize : nInLen * 3 + 1 >>> 2, aBytes = new Uint8Array(nOutLen);
  
	for (var nMod3, nMod4, nUint24 = 0, nOutIdx = 0, nInIdx = 0; nInIdx < nInLen; nInIdx++) {
	  nMod4 = nInIdx & 3;
	  nUint24 |= b64ToUint6(sB64Enc.charCodeAt(nInIdx)) << 18 - 6 * nMod4;
	  if (nMod4 === 3 || nInLen - nInIdx === 1) {
		for (nMod3 = 0; nMod3 < 3 && nOutIdx < nOutLen; nMod3++, nOutIdx++) {
		  aBytes[nOutIdx] = nUint24 >>> (16 >>> nMod3 & 24) & 255;
		}
		nUint24 = 0;
	  }
	}
  
	return aBytes;
  }
  
  /* Base64 string to array encoding */
  
  function uint6ToB64 (nUint6) {
  
	return nUint6 < 26 ?
		nUint6 + 65
	  : nUint6 < 52 ?
		nUint6 + 71
	  : nUint6 < 62 ?
		nUint6 - 4
	  : nUint6 === 62 ?
		43
	  : nUint6 === 63 ?
		47
	  :
		65;
  
  }
  
function UInt8ToB64 (aBytes) {
  
	var eqLen = (3 - (aBytes.length % 3)) % 3, sB64Enc = "";
  
	for (var nMod3, nLen = aBytes.length, nUint24 = 0, nIdx = 0; nIdx < nLen; nIdx++) {
	  nMod3 = nIdx % 3;
	  /* Uncomment the following line in order to split the output in lines 76-character long: */
	  /*
	  if (nIdx > 0 && (nIdx * 4 / 3) % 76 === 0) { sB64Enc += "\r\n"; }
	  */
	  nUint24 |= aBytes[nIdx] << (16 >>> nMod3 & 24);
	  if (nMod3 === 2 || aBytes.length - nIdx === 1) {
		sB64Enc += String.fromCharCode(uint6ToB64(nUint24 >>> 18 & 63), uint6ToB64(nUint24 >>> 12 & 63), uint6ToB64(nUint24 >>> 6 & 63), uint6ToB64(nUint24 & 63));
		nUint24 = 0;
	  }
	}
  
	return  eqLen === 0 ?
		sB64Enc
	  :
		sB64Enc.substring(0, sB64Enc.length - eqLen) + (eqLen === 1 ? "=" : "==");
  
}