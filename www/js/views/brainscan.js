export {
	BrainScanner as default
};

class BrainScanner extends HTMLElement {
	constructor(terranium,start) {
		super();
		this._ = {};

		this.attachShadow({'mode':'open'});
		this.shadowRoot.innerHTML = '<style>:host{vertical-align: middle;} table{display:inline-block;border:1px solid slategray; border-radius:0.3em;} td{font-size:0.3em;height:1em;width:1em;border-radius:1em;border:1px solid slategray;background-color:rgb(0,0,0,0);} td.on{background-color:rgb(0,0,0,1);}</style><table><tbody></tbody></table>';
		this._.body = this.shadowRoot.querySelector('tbody');

		this._.adder = (e)=>{
			this.newCritter(e.detail);
		};
		this._.remover = (e)=>{
			this.remCritter(e.detail);
		};
		this._.updater = (e)=>{
			this.listChange(e);
		};
		this._.progress = (e)=>{
			this.Render();
		};
		this._.scalewatcher = (e)=>{
			if(e.detail.includes('reward')){
				if(e.target.reward > this._.highExp || e.target.reward > this._.lowExp){
					this._.highExp = Math.max(e.target.reward, this._.highExp);
					this._.lowExp = Math.min(e.target.reward, this._.lowExp);
					for(let row of this._.body.rows){
						this.RenderReward(row);
					};
				}
			}
		};

		this._.highExp = 0;
		this._.lowExp = 0;

		this.addEventListener('click', () => {
			this.Toggle();
		});

		this.terranium = terranium || null;
		if(typeof start !== 'undefined'){
			this.play = start;
		}
		this.Render(true);
		setInterval(this._.progress,2000);
	}

	get terranium(){
		return this._.terranium;
	}
	set terranium(value){
		if(value === this._.terranium) return;
		value = value || null;

		// stop listennig for events on the old one
		if(this._.terranium){
			this._.terranium.removeEventListener('add',this._.adder);
			this._.terranium.removeEventListener('remove',this._.remover);
			this._.terranium.lifeForms.forEach(l=>{this.remCritter(l);});
		}

		this._.terranium = value;

		// start listennig for events on the new one
		if(this._.terranium){
			this._.terranium.addEventListener('add',this._.adder);
			this._.terranium.addEventListener('remove',this._.remover);
			this._.terranium.lifeForms.forEach(l=>{this.newCritter(l);});
		}

		// turn the animation on and off, just to reset some of the visualizations
		this.Toggle();
		this.Toggle();
	}

	get play(){
		return this.hasAttribute('play');
	}
	set play(animate){
		animate = !!animate;
		if(this.play === animate) return;

		if (animate) {
			this.setAttribute('play', '');
			if(this._.terranium){
				this._.terranium.addEventListener('change',this._.updater);
				this._.terranium.addEventListener('progress',this._.progress);
			}
		}
		else {
			this.removeAttribute('play');
			if(this._.terranium){
				this._.terranium.removeEventListener('change',this._.updater);
				this._.terranium.removeEventListener('progress',this._.progress);
			}
		}
		this._.requestCallBack = null;
		this.Render(true);
	}

	Start(animate=true){
		let oldstate = this.animate;
		this.play = animate;
		return oldstate;
	}

	Stop(){
		return this.Start(false);
	}

	Toggle(){
		return this.Start(!this.play);
	}

	Render() {
		let body = this._.body;
		Array.from(body.rows).forEach((tr,i)=>{
			let life = tr.critter;
			let c = 3;
			life.sensors.forEach((input,i)=>{
				if(input){
					tr.cells[c].classList.add('on');
				}
				else{
					tr.cells[c].classList.remove('on');
				}
				c++;
			});
			c++;
			tr.cells[c].style.backgroundColor = '#' + life.brain.hash.toString(16);
			c++;
			c++;
			life.actuators.forEach((output,i)=>{
				if(output){
					tr.cells[c].classList.add('on');
				}
				else{
					tr.cells[c].classList.remove('on');
				}
				c++;
			});
		});
	}

	RenderReward(tr){
		let life = tr.critter;
		let pos = this.terranium.best.experience.positive;
		let neg = this.terranium.worst.experience.negative;

		let goodness = (pos===0)?0:life.experience.positive/pos;
		let badness  = (neg===0)?0:life.experience.negative/neg;
		let avgGoodness = (goodness - badness) / 2;
		let color = d3.interpolateRdYlBu(avgGoodness + 0.5);
		tr.cells[1].setAttribute('title', "score: "+life.reward);
		tr.cells[1].style.backgroundColor = color;
	}

	newCritter(critter){
		if(!critter.sensors) return;

		let tr = document.createElement('tr');
		this._.body.append(tr);

		tr.setAttribute('name',critter.id);
		tr.critter = critter;
		tr.innerHTML = '<td></td>'.repeat(6+critter.sensors.length+critter.actuators.length);

		let kolor = critter.experience.karma || 0;
		kolor /= (critter.MAX_REWARD*2);
		kolor /= 2;
		kolor = d3.interpolatePuBuGn(kolor);

		tr.cells[0].setAttribute('title', "karma: " + critter.experience.karma);
		tr.cells[0].style.backgroundColor = kolor;

		tr.cells[2].style.opacity = 0;
		tr.cells[3+critter.sensors.length].style.opacity = 0;
		tr.cells[tr.cells.length-critter.actuators.length-1].style.opacity = 0;

		tr.changer = (event)=>{
			event.detail.forEach(field=>{
				if(field === 'reward'){
					this.RenderReward(tr);
				}
			});
		};
		critter.addEventListener('change',tr.changer);
	}

	remCritter(critter){
		let tr = this._.body.querySelector(`tr[name="${critter.id}"]`);
		if (!tr) return;
		critter.removeEventListener('change',tr.changer);
		tr.remove();
	}

	listChange(e){
		let events = e.detail.filter(d=>{
			d = (d === 'best' || d === 'worst');
			return d;
		});
		if(events.length > 0){
			if(this._.best) this._.best.removeEventListener('change',this._.scalewatcher);
			if(this._.worst) this._.worst = removeEventListener('change',this._.scalewatcher);
			this._.best = e.target.best;
			this._.worst = e.target.worst;
			e.target.best.addEventListener('change',this._.scalewatcher);
			e.target.worst.addEventListener('change',this._.scalewatcher);
		}
	}
}
customElements.define('life-brainscan', BrainScanner);
