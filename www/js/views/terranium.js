import Bug from '../things/bugs.js';

export {
	Terranium as default
};

class Terranium extends HTMLElement {
	constructor(terranium,start) {
		super();
		this._ = {};

		this.attachShadow({'mode':'open'});
		this.shadowRoot.innerHTML = '<style></style><svg width="600px" height="600px" viewBox="0 0 32768 32768"></svg>';

		this._renderarea = this.shadowRoot.querySelector('svg');
		this._style = this.shadowRoot.querySelector('style');
		this._style.innerHTML = Terranium.defaultCSS;

		this.addEventListener('click', () => {
			this.Toggle();
		});

		this.terranium = terranium || null;
		if(typeof start !== 'undefined'){
			this.play = start;
		}
		this.Render(true);
	}

	get SCALE(){
		return 32767;
	}
	get PIXEL(){
		return Math.round(this.SCALE/512);
	}
	get terranium(){
		return this._.terranium;
	}
	set terranium(value){
		if(value === this._.terranium) return;

		value = value || null;
		this._.terranium = value;

		this.Toggle();
		this.Toggle();
	}

	get play(){
		return this.hasAttribute('play');
	}
	set play(animate){
		animate = !!animate;
		if(this.play === animate) return;

		if (animate) {
			this.setAttribute('play', '');
		}
		else {
			this.removeAttribute('play');
			clearTimeout(this._.requestCallBack);
			this._.requestCallBack = null;
		}
		this._.requestCallBack = null;
		this.Render(true);
	}

	Start(animate=true){
		let oldstate = this.animate;
		this.play = animate;
		return oldstate;
	}

	Stop(){
		return this.Start(false);
	}

	Toggle(){
		return this.Start(!this.play);
	}

	Render() {
		let terranium = this._renderarea.querySelector('[name="terranium"]');
		if(!terranium){
			this._renderarea.innerHTML = "<g name='life'></g>";
			terranium = document.createElementNS('http://www.w3.org/2000/svg','circle');
			this._renderarea.prepend(terranium);
			terranium.setAttribute('name','terranium');
			terranium.setAttribute('fill','WhiteSmoke');
			terranium.setAttribute('stroke','black');
			terranium.setAttribute('stroke-width',this.PIXEL);
			terranium.setAttribute('cx',this.SCALE*0.5);
			terranium.setAttribute('cy',this.SCALE*0.5);
			terranium.setAttribute('r',this.SCALE*0.5);
		}

		if(this.terranium){
			// draw the things
			this.RenderLifeForms();
			this.RenderBugs();

			if(!this._.requestCallBack){
				this._.requestCallBack = setTimeout(()=>{
					this._.requestCallBack = window.requestAnimationFrame(()=>{
						this._.requestCallBack = null;
						this.Render();
					});
				},(this.play)?0:5000);
			}
		}
	}

	mapLife(a,d){
		let key = d.id.toString() || d.getAttribute('name');
		a[key] = d;
		return a;
	}

	RenderLifeForms() {
		let lifeforms = this.terranium.lifeForms.reduce(this.mapLife,{});

		let container = this._renderarea.querySelector('g[name="life"]');
		let visuals = Array.from(this._renderarea.querySelectorAll('g[name="life"] > g.lifeform'));
		visuals = visuals.reduce(this.mapLife,{});
		let removals = Object.assign({},visuals);

		for(let vis of Object.keys(visuals)){
			if(vis in lifeforms){
				delete removals[vis];
				delete lifeforms[vis];
			}
		}
		for(let life of Object.values(lifeforms)){
			let g =  document.createElementNS('http://www.w3.org/2000/svg','g');
			container.append(g);
			g.classList.add('lifeform');
			g.classList.add(life.Type);
			g.setAttribute('name',life.id);
			g.obj = life;

			if(life instanceof Bug){
				g.innerHTML = "<line class='antenna' ></line>".repeat(3);
				let a = Array.from(g.querySelectorAll('line.antenna'));
				a.forEach((a)=>{
					a.setAttribute('stroke','black');
					a.setAttribute('stroke-width',this.PIXEL);
					a.setAttribute('x1',0);
					a.setAttribute('y1',0);
					a.setAttribute('x2',0);
					a.setAttribute('y2',0);
					a.classList.add('antenna');
				});

				a[0].setAttribute('x2',life.antennaLen*this.SCALE   );
				a[1].setAttribute('y2',life.antennaLen*this.SCALE   );
				a[2].setAttribute('y2',life.antennaLen*this.SCALE*-1);
			}

			let c = document.createElementNS('http://www.w3.org/2000/svg','circle');
			c.setAttribute('fill','ForestGreen');
			c.setAttribute('stroke','black');
			c.setAttribute('stroke-width',this.PIXEL);
			c.setAttribute('r' ,Math.floor(life.r * this.SCALE));
			g.append(c);

			visuals[life.id] = g;
			delete removals[life.id];
		}

		// Remove extraneous objects
		for(let vis of Object.values(removals)){
			vis.remove();
		}

		// Move all the objects to their position
		for(let vis of Object.values(visuals)){
			let pos = 'translate({x},{y}) rotate({a})'
				.replace('{x}', Math.floor(vis.obj.x * this.SCALE))
				.replace('{y}', Math.floor(vis.obj.y * this.SCALE))
				.replace('{a}', Math.floor(vis.obj.dir * 360))
				;
			let oldpos = vis.getAttribute('transform');
			if(pos !== oldpos){
				vis.setAttribute('transform',pos);
			}
		}

	}


	RenderBugs(){
		let bugs = this._renderarea.querySelectorAll('.Bug');
		let pos = this.terranium.best.experience.positive;
		let neg = this.terranium.worst.experience.negative;
		for (let bug of bugs){
			this.bug(bug,pos,neg);
		}
	}


	bug(bug,pos,neg) {
		// I have made the color calculation really explicit because
		// it is confusing the first time you see it. This is slower,
		// but (hopefully) more explanitory
		let goodness = !pos ? 0 : bug.obj.experience.positive/pos;
		let badness = !neg ? 0 : bug.obj.experience.negative/neg;
		let avgGoodness = (goodness - badness) / 2;
		let color = d3.interpolateRdYlBu(avgGoodness + 0.5);
		bug.querySelector('circle').setAttribute('fill',color);
	}
}
customElements.define('life-terranium', Terranium);



		/*
const cAquarium = 'WhiteSmoke';
const cLifeBorder = 'SlateGray';
const cFoodColor = 'forestgreen';
const cAntena = 'DarkSlateGray';
	*/

Terranium.defaultCSS = `
	:host{
		vertical-align: middle;
	}
	[name='terranium'] {
		fill: WhiteSmoke;
		stroke: SlateGray;
	}
	line.antenna {
		stroke: SlateGray;
	}
	.lifeform circle {
		stroke: SlateGray;
	}
`;
